import React from 'react';
import Colors from './Colors';
import PropTypes from 'prop-types';

export default class Palette extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			currentColor: null,
			mixPercent: 10
		};

		this.selectColor = this.selectColor.bind(this);
	}

	selectColor(color) {
		if (!this.state.currentColor) {
			this.setState({currentColor: color});
		} else {
			let currentColor = this._toHexadecimal(this.state.currentColor);
			color = this._toHexadecimal(color);
			currentColor = this._mixColors(currentColor, color);
			currentColor = this._toAscii(currentColor);
			this.setState({currentColor: currentColor});
		}
	}

	_toHexadecimal(colorCode) {
		return {
			r: parseInt(colorCode.substring(1, 3), 16),
			g: parseInt(colorCode.substring(3, 5), 16),
			b: parseInt(colorCode.substring(5, 7), 16)
		};
	}

	_toAscii(rgb) {
		return '#' + rgb.r.toString(16) + rgb.g.toString(16) + rgb.b.toString(16);
	}

	_mixColors(baseColor, addColor) {
		let percent = this.state.mixPercent / 100;
		return {
			r: this._combineColor(baseColor.r, addColor.r, percent),
			g: this._combineColor(baseColor.g, addColor.g, percent),
			b: this._combineColor(baseColor.b, addColor.b, percent)
		};
	}

	_combineColor(colorA, colorB, percent) {
		colorA = colorA * (1 - percent);
		colorB = colorB * percent;

		return parseInt(colorA + colorB);
	}

	render() {
		return <div className="palette">
			<Colors colors={this.props.colors} selectColor={this.selectColor}/>
			{this.renderMixer()}
		</div>;
	}

	renderMixer() {
		let content = <p>Please select the base color</p>;
		let style = {};
		if (this.state.currentColor) {
			style.backgroundColor = this.state.currentColor;
			content = null;
		}

		return <div className="mixer" style={style}>
			{content}
		</div>;
	}
}

Palette.propTypes = {
	colors: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired
	})).isRequired
};
