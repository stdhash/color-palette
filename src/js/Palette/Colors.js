import React from 'react';
import PropTypes from 'prop-types';

export default class Colors extends React.Component {
	render() {
		return <div className="colors">
			{this.renderColors()}
		</div>;
	}

	renderColors() {
		let i = 0;
		let iLen = this.props.colors.length;
		let colors = [];

		for (; i < iLen; ++i) {
			let color = this.props.colors[i];
			let style = {
				backgroundColor: color.id,
			};

			colors.push(<div
				key={color.id}
				alt={color.name}
				className="color"
				style={style}
				onClick={() => this.props.selectColor(color.id)}/>
			);
		}

		return colors;
	}
}

Colors.propTypes = {
	colors: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired
	})).isRequired,
	selectColor: PropTypes.func.isRequired
};
