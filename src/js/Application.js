import React from 'react';
import Palette from './Palette';
import {colors} from './conf';

export default class Application extends React.Component {
	render() {
		return <div>
			<Palette colors={colors}/>
		</div>;
	}
}
