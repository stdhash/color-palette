export const colors = [
	{
		id: '#FF0000',
		name: 'Red'
	},
	{
		id: '#0000FF',
		name: 'Blue'
	},
	{
		id: '#FFFF00',
		name: 'Yellow',
	},
	{
		id: '#008000',
		name: 'Green'
	},
	{
		id: '#000000',
		name: 'Black'
	},
	{
		id: '#FFFFFF',
		name: 'White'
	}
];
